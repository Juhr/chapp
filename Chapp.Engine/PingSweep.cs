﻿using System;
using System.Net.NetworkInformation;

namespace Chapp.Engine
{
	// Sweeps the local network for devices that respond to a ping.
	public class PingSweep
	{
		private static string FORMAT_IP = "192.168.{0}.{1}";

		private int pingCount;
		private int sectionThree;
		private int sectionFour;
		private bool sweepFinished;
		private object pingLock = new object();

		public event EventHandler<PingSuccessEventArgs> PingSuccess;

		// Create the ping sweep and collect the host addresses.
		public PingSweep(int concurrentPings)
		{
			this.pingCount = concurrentPings;
			this.sweepFinished = true;
		}

		// Sweep the local area for ip addresses.
		public void Sweep()
		{
			if (!this.sweepFinished)
			{
				throw new PingException("Cannot perform a ping sweep while one is already in progress.");
			}
			this.sectionThree = 0;
			this.sectionFour = 0;
			this.sweepFinished = false;

			// Start all available pings
			for (int i = 0; i < this.pingCount; i++)
			{
				var ping = new Ping();
				ping.PingCompleted += this.Ping_PingCompleted;
				this.PingNext(ping);
			}
		}

		// Cancel a sweep in progress.
		public void CancelSweep()
		{
			this.sweepFinished = true;
		}

		// Perform the next ping.
		private void PingNext(Ping ping)
		{
			lock (pingLock)
			{
				if (this.sweepFinished)
				{
					ping.Dispose();
					return;
				}

				// Send the ping on its way
				ping.SendAsync(String.Format(FORMAT_IP, this.sectionThree, this.sectionFour), ping);

				// Increment the address
				this.sectionFour++;
				if (this.sectionFour >= 255)
				{
					this.sectionFour = 0;
					this.sectionThree++;
					if (this.sectionThree >= 256)
					{
						this.sweepFinished = true;
					}
				}
			}
		}

		// Fired when a ping has completed, stores the succcessful ping response in the addresses list.
		private void Ping_PingCompleted(object sender, PingCompletedEventArgs e)
		{
			// Send out if successful
			if (e.Reply.Status == IPStatus.Success && !this.sweepFinished && this.PingSuccess != null)
			{
				this.PingSuccess(this, new PingSuccessEventArgs(e.Reply.Address));
			}

			// Use the now-free ping again
			this.PingNext((Ping)e.UserState);
		}
	}
}
