﻿using System;
using System.Net;

namespace Chapp.Engine
{
    // Event args for the ping success event of PingSweep.
    public class PingSuccessEventArgs : EventArgs
    {
        private IPAddress address;

        // Create the event args.
        public PingSuccessEventArgs(IPAddress address)
        {
            this.address = address;
        }

        public IPAddress Address
        {
            get { return this.address; }
        }
    }
}
