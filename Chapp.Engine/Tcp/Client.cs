﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Chapp.Engine.Tcp
{
    // A tcp client for local networking.
    public class Client
    {
        private static int PORT = 2055;

        private TcpClient client;
        public StreamWriter writer;

        public Client()
        {
            this.client = new TcpClient();
        }

        // Attempts to connect to the given ip, sets up stream if successful.
        public bool AttemptConnection(IPAddress ip)
        {
            try
            {
                this.client.Connect(ip, PORT);
            }
            catch (Exception)
            {
                return false;
            }
            if (!this.client.Connected)
            {
                return false;
            }

            // Set up the stream
            this.writer = new StreamWriter(this.client.GetStream());
            this.writer.AutoFlush = true;

            return true;
        }
    }
}
