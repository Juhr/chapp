﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Chapp.Engine.Tcp
{
	// A tcp server for local networking.
	public class Server
	{
		private static int PORT = 2055;

		private TcpListener listener;
		private Thread listenThread;

		// Create a new server.
		public Server()
		{
			this.listener = new TcpListener(IPAddress.Any, PORT);
			this.listenThread = new Thread(new ThreadStart(this.Listen));
		}

		// Start the listening process.
		public void BeginListening()
		{
			this.listener.Start();
			this.listenThread.Start();
		}

		// End the listening process.
		public void EndListening()
		{
			this.listener.Stop();
			this.listenThread.Abort();
		}

		// Sleeps and checks for incoming connections
		private void Listen()
		{
			try
			{
				while (true)
				{
					Thread.Sleep(500);
					Console.WriteLine("Listening...");
					if (this.listener.Pending())
					{
						// Attempt to connect
						Console.WriteLine("Connecting to socket");
						var socket = this.listener.AcceptSocket();

						Console.WriteLine("Connected.");
						this.ReadStream(socket);
						break;
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("Aborting Server. Error: {0}", e.Message);
			}
		}

		// Read the stream from the given socket.
		private void ReadStream(Socket socket)
		{
			using (var stream = new NetworkStream(socket))
			{
				var reader = new StreamReader(stream);
				while (!reader.EndOfStream)
				{
					Console.WriteLine(reader.ReadLine());
				}
				Console.WriteLine("Disconnecting");
			}
		}
	}
}
