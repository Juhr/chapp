﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Chapp.Console
{
    // Provides a local network implementation of a Tcp client and server.
    public class LocalTcp
    {
        private static int PORT = 2055;
        private bool isServer;

        // Server variables
        private TcpListener listener;

        // Client variables
        private TcpClient client;

        public void RunAsServer()
        {
            this.listener = new TcpListener(IPAddress.Any, PORT);

            this.listener.Start();

            var socket = this.listener.AcceptSocket();

            using (var stream = new NetworkStream(socket))
            {
                var reader = new StreamReader(stream);
                while (!reader.EndOfStream)
                {
                    System.Console.WriteLine(reader.ReadLine());
                }
            }
            
            socket.Close();

            this.listener.Stop();
        }

        public void RunAsClient()
        {
            // Find all the local ips available by ping
            IPAddress[] addresses = null;// new PingSweep().SweepLocal();
            
            // Create the client and attempt to connect to any of the addresses
            this.client = new TcpClient();
            for (int i = 0; i < addresses.Length; i++)
            {
                
                try
                {
                    this.client.Connect(addresses[i], PORT);
                }
                catch (Exception e)
                {
                    System.Console.WriteLine(e.Message);
                }

                if (this.client.Connected)
                {
                    System.Console.WriteLine("Connected to: {0}", addresses[i]);
                    break;
                }
                System.Console.WriteLine("Could not connect to: {0}", addresses[i].ToString());
            }
            if (!this.client.Connected)
            {
                return;
            }

            using (var stream = this.client.GetStream())
            {
                var writer = new StreamWriter(stream);
                writer.AutoFlush = true;

                System.Console.WriteLine("Enter your message...");
                writer.WriteLine(System.Console.ReadLine());
            }

            this.client.Close();
        }
    }
}
