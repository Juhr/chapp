﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Chapp.Engine;
using Chapp.Engine.Tcp;

namespace Chapp.Console
{
	class Program
	{
		private static int PORT = 2055;
		private TcpListener listener;
		private Client client;
		private PingSweep ping;
		private Server server;
		private bool connected;

		static void Main(string[] args)
		{
			while (true)
			{
				System.Console.WriteLine("s or c");
				var response = System.Console.ReadLine();
				if (response == "s")
				{
					new Program().RunServer();
					break;
				}
				else if (response == "c")
				{
					new Program().RunClient();
					break;
				}
			}
		}

		private void Ping_PingSuccess(object sender, PingSuccessEventArgs e)
		{
			if (!this.connected)
			{
				System.Console.WriteLine(e.Address);
				if (this.client.AttemptConnection(e.Address))
				{
					this.connected = true;
					this.ping.CancelSweep();
					System.Console.WriteLine(true);
				}
			}
		}
		
		public void RunServer()
		{
			this.server = new Server();
			this.server.BeginListening();

			System.Console.ReadLine();
		}

		public void RunClient()
		{
			this.connected = false;
			this.client = new Client();
			this.ping = new PingSweep(20000);
			this.ping.PingSuccess += this.Ping_PingSuccess;
			this.ping.Sweep();

			while (true)
			{
				if (this.connected)
				{
					this.ClientLoop();
					break;
				}
			}
		}

		public void ClientLoop()
		{
			while (true)
			{
				System.Console.WriteLine("Enter message below ('q' to quit):");
				var response = System.Console.ReadLine();
				if (response == "q")
				{
					break;
				}
				this.client.writer.WriteLine(response);
			}
		}
	}
}
